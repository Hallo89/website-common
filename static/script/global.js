(function() {

window.addEventListener('load', load);

// ---- Immediate setup ----
(function() {
  if (document.querySelector('[data-has-theme]')) {
    let preferredTheme = localStorage.getItem('Global_theme');
    if (!preferredTheme) {
      preferredTheme = window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
    }
    toggleTheme(preferredTheme);
  }
})();

// ---- Setup on load ----
function load() {
  setupComponentBoxes();
  setupTheme();
}

function setupComponentBoxes() {
  // Add a custom href to the buttons in component boxes (since anchors can't be nested)
  const componentBoxes = document.querySelectorAll('.gbl-box.gbl-components');
  for (const box of componentBoxes) {

    const quickLinkBtns = box.querySelectorAll('.quick-links button');
    for (const button of quickLinkBtns) {
      button.addEventListener('click', function(e) {
        e.preventDefault();
        window.location.href = this.dataset.href;
      });
    }
  }
}
function setupTheme(argument) {
  const themeToggleButton = document.querySelector('#theme-toggle .theme-toggle-btn');
  if (themeToggleButton) {
    themeToggleButton.addEventListener('click', () => {
      toggleThemeAndSetCookie();
    });
  }
}

// ---- Theme handling ----
function toggleThemeAndSetCookie(theme) {
  toggleTheme(theme);
  updateLocalStorageTheme();
}

function toggleTheme(theme) {
  const root = document.documentElement;

  if (theme != null) {
    if (theme === 'light') {
      root.classList.add('light-mode');
    } else if (theme === 'dark') {
      root.classList.remove('light-mode');
    }
  } else {
    root.classList.toggle('light-mode');
  }
}

function updateLocalStorageTheme() {
  localStorage.setItem('Global_theme', document.documentElement.classList.contains('light-mode') ? 'light' : 'dark');
}

}());
